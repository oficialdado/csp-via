#!/bin/bash

# Start CSP
JVM_PARAMS="-Dsun.net.inetaddr.ttl=0 -d64 -XX:+UseParallelOldGC -Xms$JAVA_MEMORY_MIN -Xmx$JAVA_MEMORY_MAX -Xloggc:/tmp/csp1gc.log -XX:+PrintGCDetails"
cd /usr/local/cardservproxy && java "$JVM_PARAMS" -jar lib/cardservproxy.jar > log/cardserv-sysout.log 2>&1

